<?php

namespace WP\Core\Hooks;


interface Action {

	public function add_actions();
}