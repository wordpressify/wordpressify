<?php

namespace WP\Core\Hooks;


interface Filter {
	public function add_filters();
}